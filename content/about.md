---
title: About Bright Open
subtitle: Bright Open solutions, ideas, visions
---

# Website

* [brightopen.eu](https://brightopen.eu)
* [brightopen.cz](https://brightopen.cz)

# Members

* Jo Cutajar
