---
title: The JSON extension for log4net has found a new home
date: 2021-03-24
tags: ["log4net", "json", "c-sharp", "dotnet"]
categories: ["announcement"]
---

The [log4net.Ext.Json](https://www.nuget.org/packages/log4net.Ext.Json/) [I wrote about a while back]({{< ref "/post/2021-02-07-json-extension-for-log4net-to-go.md" >}}) is now safely settled in a new home.
The [call for maintainers](https://gitlab.com/BrightOpen/log4net.Ext.Json/-/issues/9) has been closed and
[Grzegorz Dziadkiewicz has successfully taken over](https://gitlab.com/gdziadkiewicz/log4net.Ext.Json).
This is great! I bless it so it will serve well and bring more satisfaction. Many thanks to Grzegorz!
