---
title: The JSON extension for log4net is ready to go
date: 2021-02-07
tags: ["log4net", "json", "c-sharp", "dotnet"]
categories: ["announcement"]
---

This package [log4net.Ext.Json](https://www.nuget.org/packages/log4net.Ext.Json/) has brought home a lot of satisfaction.
But since I'm not developing nor managing any dotnet software anymore, I do not have the incentive to continue
maintaining and supporting it. The last and most popular version is 2.0.8.3.
<!--more-->
At the time of it's inception, it was quite a rare piece. I believe there are
similar solutions nowadays. Still, this [package](https://www.nuget.org/stats/packages/log4net.Ext.Json?groupby=Version):

 * has nearly reached a 1M downloads, 
 * averaging at 363 per day, 
 * 45k last 6 weeks... 

All that even though I haven't touched it in three years. I'm impressed and humbled :) It is time to let go and let someone else take over. I am [calling for maintainers here](https://gitlab.com/BrightOpen/log4net.Ext.Json/-/issues/9).