+++
title = "Bright Open is Official"
date = 2021-11-02T00:58:34+01:00
tags = ["brightopen"]
categories = ["announcement"]
draft = false
toc = false
backtotop = false
+++

The Bright Open s.r.o. company has been registered in CZ and got an official website [brightopen.eu](https://brightopen.eu).

<!--more-->

[The registry listing](https://or.justice.cz/ias/ui/rejstrik-firma.vysledky?subjektId=1138202&typ=PLATNY) has all the relevant details. Let's go make some business...

