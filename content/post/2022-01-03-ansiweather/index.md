---
title: "Console: weather forecast"
date: 2022-01-03T16:00:34+01:00
tags: ["console", "experience"]
categories: ["blog"]
draft: false
toc: false
backtotop: false
---

# What's the weather?

Today I've explored a utility to display weather information and forecast on the text console or terminal. Surely, you can have those things on your mobile device or integrated in your fancy desktop environment. If you're like me, into minimal stuff, eliminating clutter and distractions, consider [**`ansiweather`**](https://github.com/fcambus/ansiweather). 

# Installation on Debian+

It is in the repos so any Debian based distro should have it. Watch my lingo :)

```bash
sudo apt install ansiweather
```

# Usage

The tool is well documented with a man page. You can just run `ansiweather` or `ansiweather -F` to get started.

```bash
man ansiweather
```

# Multi-line display that updates itself

Here I take advantage of other console tools to transform the output into multiple lines.

* `watch` - periodically run a command and display the output, let's say every five minutes.
* `sed` - command line text editor, here we use the search and replace feature with regular expressions.

With this equipment, you can have the weather forecast and info always fresh on your desk. For fancy effect, increase the font size and notice the cute weather symbols. This will be one of the tiles on my info tag in [Awesome WM](https://awesomewm.org/) or [VTM](https://vtm.netxs.online/)

```bash
LOCATION=Pilsen,CZ
watch -ctn 300 "ansiweather -l '$LOCATION' -d true -a false -s true | 
    sed -r 's/ - /\n/g' | 
    sed -r 's/=> /\n\t/';
    echo; 
    ansiweather -l '$LOCATION' -F -a false -s true | 
    sed -r 's/=> | - /\n/g'"
```

![screenshot](tiles.png)
