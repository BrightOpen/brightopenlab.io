---
title: "Song: To me"
date: 2022-10-18T21:00:34+02:00
tags: ["songs", "faith"]
categories: ["blog"]
draft: false
toc: false
backtotop: false
---

Here's a little song I wrote... No, this is not a story about Bobby McFerrin and his dear friend Robin Williams. But there is joy and pain, death and life, love and despair. And a question of faith.

<!--more-->

# Song: To me

```
Burn it with fire - your guilt, your shame and all the pain.
Rise above the ashes of your fear.
Drink from the well of joy and love - the holly grail.
Part with doubt - there's no one else to steer.

Give mother Earth - your grudge, your spite and vengence dreams.
Let her turn them into gifts for all.
Give her your thanks - for life, for beauty, abundance...
Share the news - love is the only law!

Whisper to wind - your hopes and wishes of your heart.
Make your life one never ending prayer.
Watch out for signs of chance to make this world a bit better.
Start with yourself, now and then forever.
```

# The process

This song was inspired with the visit of a friend and lover dear to me. We watched "The power of the heart" and discussed many things... We even dared to go busking in the park. And when she left, I was in a different state - mentally. Karel Kryl's song "Morituri te salutant" eventually popped into my mind, but with a different, more joyful and groovy tune. I liked the new tune a lot, but the text did not match the mood anymore and a new song started budding. I sent the verses to my friend as they came. I could not stop until it was done. And I recorded the tune for her, too.

# The after-thought

I think the inspiration came as my sub-conscious attempt to advise my friend based on our discussions, but later I realized I better advise myself actually so I called it "To me" as it has a lot of imperative suggestions I ought to tell the "man in the mirror". The friendly and delightful mirror...

In the "The power of the heart", they mention a gospel song "Leave it there". Surely also part of the inspiration. I was curious about it. It took me on a journey of discovery. And now I'm having some difficult thoughts.

There was this lovely God-believing couple - Joey and Rory - very upfront about their religion, never doubting, running a happy marriage. Claiming God had a plan for them. A wonderful country duo band, too. Before their marriage, he was divorced and was a single dad of two girls. After a good few years of this God-blessed marriage and a successful musical career, she died of cervical cancer and he was left with a third girl, this one with a Down syndrome. And there goes [the song "Leave it there" they covered](https://www.youtube.com/watch?v=HFuG8C3yGx0) which speaks of healing oneself through undoubting faith. And yet, it seems that the opposite was the case for Joey and Rory. As if the God they prayed to was trying just how firm their undoubting faith was. How could anyone claim that faith alone will heal someone in the light of this profound story? And if it is true, was it after all the correct God they prayed to?

This is the issue I have with the Biblical God. Even according to the Bible to my mind, he is perfectly nasty - jealous, vengeful and pervert. It's quite a different picture than my experience of divine love. The Biblical God must be a God people made. Or some mad being of this world and yet above humans. I cannot reconcile the two. And I understand that Jesus and many other teachers brought us closer to the divine love, though he was talking about the Biblical "Lord". That is confusing. It's a mess - for the reasonable mind - unless you consider his circumstances.

And yet, the water from the well of joy and love is so clear to me. It doesn't promise to heal me or protect me. It doesn't give me rules to follow and punishments for misconduct. It doesn't scold me for my imperfection. It just shows me the way like rivers run to the seas. And if I don't follow the way, it doesn't get offended at all. It is full of joy and love again when I return. Never drying out. If I'm thirsty, it's only because I did not care to drink. And this well of truth shows me that I surely will die and I will hurt and that it's all well part of life. And it shows me how to accept that and not dwell in misery and pity. And it tells me how to take care of myself (when I care to listen) so that I may have a more joyful ride. And it's not upset when I'm lazy or reckless... it actually doesn't judge me at all. All of life is welcome, unconditionally.

So that's some context to the song. When it says "Give mother Earth..." you may understand it symbolicaly or metaphorically as a tool for the mind to be able to let go of thoughts that burden it. Or you may understand it quite literally, that there is a living Earth being and that she accepts the burden as a form of energy. You may understand it practically as grounding in gardening. In my experience (and I often remind my skeptical analytical mind) there is no difference. Live the mystery. And yet, between the awe and marvel, I doubt and ponder, argue with and offend my divine experience, again and again, calling it an imagination and myself a fool. So far, that's been the surest way I can be in touch with it. Nothing I do really matters and yet... the future of the universe depends on it. As ridiculous and as incredible as it gets.
