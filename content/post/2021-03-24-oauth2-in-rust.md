---
title: OAuth2 in Rust - authorization code flow for native apps
date: 2021-03-24
tags: ["rust", "oauth2"]
categories: ["blog"]
---

[OAuth2](https://oauth.net/2/) is a standard for authentication/authorization. It is implemented by big cloud providers - [Azure](https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-v2-protocols),
 [Google](https://developers.google.com/identity/protocols/oauth2), [Amazon](https://docs.aws.amazon.com/cognito/latest/developerguide/authorization-endpoint.html) - and beside many others also by [GitLab](https://docs.gitlab.com/ee/integration/oauth_provider.html) or [GitHub](https://docs.github.com/en/developers/apps/authorizing-oauth-apps).

What is is it good for? You can integrate your application with a service provider, or many of them, with one pattern. The common troubles are solved so you just focus on making business (most of the time). Mostly, a library is available so you don't even need to implement the protocol. 

<!--more-->

**So, what about Rust?** [Rust has oauth libraries](https://oauth.net/code/rust/). They are great for solving the machine-to-machine communication. But rust is [not quite GUI yet](https://www.areweguiyet.com/). And there is a big part in OAuth2 that is about human interaction - in a web GUI. 

Various providers may want to enforce their own policies, authentication options, request two factor authentication, let alone branding and so on. While there are authentication flows without the UI requirement, these are considered inferior to full human interaction. The OAuth2 standard solves this by sending the user through a browser to an authorization page of the provider. Once all the checks are done, the user's browser is sent back to the app with a redirect URI containing a temporal authorization code. The app can then use this code to redeem an access/refresh token.

The good news is that a web view binding has been added to the Rust bunch of crates. Web view wraps system's native web browser so you can make a Web GUI available in a uniform way on all supported platforms. [Wry](https://docs.rs/wry) is one such crate. With a recent addition of a `CustomProtocol` feature, I was able to make it do an OAuth2 user interaction and bring back the authorization code in a [few lines of Rust](https://github.com/tauri-apps/wry/issues/127).

**So, what does it do?** The app will open a web window taking you to the OAuth provider's auth page. You will do your user interaction there... The app sends you there with a redirect URI, the place the provider will redirect you to after completing the interaction. This redirect is then intercepted by the app and it gains the information about the authorization code. In the simple example, it just dumps the code to the console, but a smarter app would redeem a token for the code.
