---
title: Bright Open spring cleanup
date: 2021-03-23
tags: ["brightopen"]
categories: ["announcement"]
---

I have [moved all the 'Bright Open' projects to GitLab](https://gitlab.com/BrightOpen) and removed old residuals from [GitHub](https://github.com/BrightOpen).

<!--more-->

Why?

 * GitLab is community driven
 * GitLab is open source
 * GitLab has private repos for free
 * GitLab has amazing CI/CD features
 * GitHub has been acquired by Microsoft and while my perception of this company is improving over time as it embraces open source more and more, there is still a lot of bitter taste left. For me, it is not a trusted community member yet.

GitHub has been a great pioneer in connecting geeks over code. Kudos for that. And it still is, I suppose. GitLab.com simply does it better for me now. I also use it at work and it does a great service on premise, too.

