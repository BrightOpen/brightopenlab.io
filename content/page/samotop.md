---
title: Samotop
subtitle: SMTP server library for Rust
tags: ["samotop", "rust"]
date: 2021-02-06
---

Samotop is 
1) a [collection of libraries to create SMTP server solutions in Rust](https://crates.io/search?q=samotop),
2) an [SMTP server](https://crates.io/crates/samotop-server) you can run locally or deploy with docker,
3) a practical result of a desire to learn Rust, grammars, docker...
4) a manifestation of a dream...

My vision is that we the people are in charge of our data and communication. Not just as implied by GDPR and similar big talk, but physically, factually. Powerful business players worked hard to centralize mail services, chats and social networks and gave that to governments, leaked it to unauthorized parties... It's not all lost. E-mail is still ubiquitous and can be leveraged for a seamless opt out.

The bigger mission is to
1) deliver an SMTP experience, that is easy to provision, subscribe and use by non-technical people with utter focus on privacy and abuse prevention. 
2) enable instant interaction, like or better than chat experience with pending contacts, networking features, integration to open social networks and more.
3) explore new ways of working with mail - step away from the tried and tested inbox concept and explore AI, visualization, information discovery.
4) explore the options of improving e-mail infrastructure - specifically address the stuffy security and privacy concerns

I'd like to do to e-mail servers what [delta.chat](https://delta.chat) does to e-mail clients :)

The Immediate mission is to make a useful contribution, something people can use to solve their problems, something that enchants the Rust community to build a samotop user base to inspire, give feedback and contribute. If you are using samotop, I do want to hear from you :) and about your use cases.
